<?php
/**
 * 404 Page
 */
get_header(); ?>

<section class="block">
    <div class="container not-found-wrap">
        <div class="left">
            <h1 class="block-title">404</h1>
            <h2 class="block-title">DONUT ENTER</h2>
            <p>We are sorry, but this page doesn't exist.</p>
            <p>Please check entered address or go to our homepage to find infinite possibilities.</p>
            <div class="btn-wrap">
                <a class="btn btn-outline-red" href="<?= site_url('/');?>">GO HOME</a>
            </div>
        </div>

        <div class="right">
            <img src="<?=get_field('404_image', 'options')['url'];?>" alt="404 Image">
        </div>
    </div>
</section>

<?php get_footer();
