$(function () {
    initSignIn();
    initBody();
    initTestimonialVideos();
    initFeaturedJobs();
    initPDF();
});

function initBody() {
    var $body = $('body');
    var result = document.title.replace("&", "").replace("'", "").replace("\"", "");
    $body.addClass(result.split(" ").join("_").toLowerCase());

    if ($body.hasClass("talentlanding-page")) {
        setTimeout(function () {
            window.scrollTo(0, 0);
        }, 1000)
    }
}

function initFeaturedJobs() {
    if ($(body).hasClass("home-page") && $('.featuredjobs').length > 0) {
        $('.featuredjobs').prev().addClass("featuredjobs");
    }
}

function initTestimonialVideos() {
    //create lightbox
    if (!$(body).hasClass("locations")) {
        var lightbox = "<div style='display:none;' class='lightbox'><div class='mask'></div><div class='container'><a class='close' href='javascript:void(0);'><i class='fas fa-times'></i></a><div class='video'><iframe allow='autoplay' width='420' height='315'frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div></div></div>";
        $('body').append(lightbox);

        $('.search + .buttontext a, .threeimagecaption a').on('click', function (e) {
            if ($(this).attr('href').indexOf('youtube.com') !== -1) {
                e.preventDefault();
                var src = $(this).attr("href"); // + "&autoplay=1";
                $('.lightbox iframe').attr("src", src);
                $('.lightbox').css("display", "block");
                return false;
            }
        });


        $('.lightbox').on("click", function (e) {
            if ($(this).css('display') == "block") {
                if ($('.lightbox .video').has(e.target).length === 0 && !$('.lightbox .video').is(e.target)) {
                    $('.lightbox iframe').attr('src', null);
                    $('.lightbox').css("display", "none");
                }
            }
        });
    }
}

function initPDF() {
    if ($(body).hasClass("locations")) {
        $('.search + .buttontext .btn').on('click', function (e) {
            e.preventDefault();
            $('.lightbox').css("display", "block");
        });

        $('.lightbox').on("click", function (e) {
            if ($(this).css('display') == "block") {
                if ($('.lightbox .image, .lightbox .download').has(e.target).length === 0 && !$('.lightbox .image, .lightbox .download').is(e.target)) {
                    $('.lightbox').css("display", "none");
                }
            }
        });
    }
}

function initSignIn(){
    $('#footer #footerRowTop #footerInnerLinksSocial .inner li:last-of-type a, .menu.desktop.upper .inner>ul>li:last-of-type a, .menu.mobile .nav .dropdown.mobile-nav .dropdown-menu li:last-of-type > a').attr("onclick", "j2w.TC.handleViewProfileAction(event)");
}