$(function () {
    var $container = $('.block.gravity-form .form-container');

    if ($container.attr('data-redirect') == "true") {
        $(document).on('gform_confirmation_loaded', function(event, formId) {
            setTimeout(function() {
                window.location.href = $container.attr('data-redirect-url');
            }, 5000);
        });
    }
});