import { CarouselProgressBar } from './carousel-progress-timer';

$(function () {
    $('.text-with-video .timed-carousel').each(function () {
        var $carousel = $(this);
        initTimer($carousel);
    });

    function initTimer($carousel) {
        var $activeCarouselItem = $carousel.find('.slick-active');
        var progressDiv = $activeCarouselItem.find('.progress-timer');
        progressDiv.show();

        var timer = new CarouselProgressBar(progressDiv, null, function () {
            $carousel.slick('slickNext');
        });

        var hasVideo = $activeCarouselItem.find('video').length > 0;

        var media = startTimer($activeCarouselItem, hasVideo, timer);

        var _currentActiveSlide = null;
        var beforeChange = function (event, slick, currentSlide, nextSlide) {
            _currentActiveSlide = currentSlide;
            if (currentSlide != nextSlide) {
                if (hasVideo) {
                    media.pause();
                    media.currentTime = 0;
                }
                timer.stop(true);
                progressDiv.hide();
                $carousel.off('beforeChange', beforeChange);
            }
        };

        var afterChange = function (event, slick, currentSlide) {
            if (_currentActiveSlide != currentSlide) {
                $carousel.off('afterChange', afterChange);
                initTimer($carousel);
            }
        };

        $carousel.on('beforeChange', beforeChange);

        $carousel.on('afterChange', afterChange);

    }

    function startTimer($activeCarouselItem, isVideo, timer) {
        if (isVideo) {
            var media = $activeCarouselItem.find('video')[0];
            media.play();
            media.ontimeupdate = function () {
                timer.setTotalTime((media.duration - media.currentTime - 0.25) * 1000);
                timer.start();
                media.ontimeupdate = null;
            }
            return media;
        } else {
            var media = $activeCarouselItem.find('img')[0];
            timer.setTotalTime(3 * 1000);
            timer.start();
            return media;
        }
    }
});