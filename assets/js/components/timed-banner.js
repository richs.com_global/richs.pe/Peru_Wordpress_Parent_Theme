import {CarouselProgressBar} from './carousel-progress-timer';

export class TimedBanner {
    constructor(element) {
        var _self = this;
        element.each(function() {
            _self.initTimedBanner($(this));
        });
    }

    initTimedBanner($carousel) {
        var _self = this;
        var progressDiv = $carousel.find('.slick-active .progress-timer');
        var timer = new CarouselProgressBar(progressDiv, {
            'time': 10000
        }, function() {
            $carousel.slick('slickNext');
        });
        timer.start();

        var _currentActiveSlide = null;
        var beforeChange = function(event, slick, currentSlide, nextSlide) {
            _currentActiveSlide = currentSlide;
            if (currentSlide != nextSlide) {
                timer.stop(true);
                $carousel.off('beforeChange', beforeChange);
            }
        };

        var afterChange = function(event, slick, currentSlide) {
            if (_currentActiveSlide != currentSlide) {
                $carousel.off('afterChange', afterChange);
                _self.initTimedBanner($carousel);
            }
        };

        $carousel.on('beforeChange', beforeChange);

        $carousel.on('afterChange', afterChange);
    }
}