$(function() {
    var body = $('body');
    var mobileNav = $('#mobile-nav');
    var mask = mobileNav.find('.mask');

    var slideOut = mobileNav.find('.mobile-nav');
    mobileNav.find('.toggle').on('click', function() {
        mobileNav.toggleClass('active');
        body.toggleClass('active');
        mask.toggleClass('active');
        slideOut.toggleClass('active');
    });

    $('#mobile-nav li .expander').on('click', function(e) {
        e.preventDefault();
        $expander = $(this);
        $parent = $expander.closest('li');
        $expander.toggleClass('expanded');
        $parent.toggleClass('expanded');
    });

    // check to see if there is a current page selected on load, and expand it if so
    var $currentPage = $('#mobile-nav li.current-menu-parent');
    if ($currentPage.length > 0) {
        $currentPage.find('.expander').trigger('click');
    }
});