$(function(){

    var $regionalLinks = $('*[data-regional-link="true"]');
    var $regionalMarketingLinks = $('*[data-regional-marketing-link="true"]');
    $regionalLinks.hide();

    window.addEventListener('rpCountryInitalized', function() {
        get_regional_links();
        get_marketing_site_links();
    });

    function get_regional_links() {
        $regionalLinks.each(function() {
            var $link = $(this);
            $link.hide();

            var globalId = $link.attr('data-global-id');
            var fallback = $link.attr('data-fallback');

            $.ajax({
                url: ajax_object.ajax_url,
                type: 'POST',
                data: {
                    'action':'get_regional_link',
                    'globalID': globalId,
                    'countryCode': window.countryCode
                },
                success: function(response) {
                    if (response.url != null) {
                        $link.attr('href', response.url);
                    } else {
                        $link.attr('href', fallback);
                    }
                },
                error: function() {
                    $link.attr('href', fallback);
                },
                complete: function() {
                    $link.show();
                }
            });
        });
    }

    function get_marketing_site_links() {
        $regionalMarketingLinks.each(function() {
            var $link = $(this);
            if (window.marketingSites != null) {
                var site = window.marketingSites[0].link;
                $link.attr('href', site.url);
                $link.attr('target', site.target);
                $link.html(site.title);
                $link.show();
                $link.parent().show();
            }
        });
    }    
});
