<?php

use Cn\{
    Theme,
    PostTypes,
    Blocks,
    Acf,
    AjaxHandler
};

/**
 * Cypress North Wordpress Bootstrap Theme
 *
 * @package  Cn
 * @author   Adam Gold <agold@cypressnorth.com>
 * @author   Brian Rizzo <brizzo@cypressnorth.com>
 */

define('THEME_VERSION', '3.0.0');
define('THEME_URL', get_template_directory_uri());
define('THEME_DIR', get_template_directory());

define('DATE_FORMAT', get_option('date_format'));
define('TIME_FORMAT', get_option('time_format'));
define('DATETIME_FORMAT', DATE_FORMAT . ' ' . TIME_FORMAT);

define('GOOGLE_API_KEY', 'your-google-api-key-here');

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/vendor/autoload.php';

Theme::init();

PostTypes::init();

Blocks::init();

Acf::init();

AjaxHandler::init();