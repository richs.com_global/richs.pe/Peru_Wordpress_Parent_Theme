<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="<?= THEME_URL; ?>/assets/dist/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= THEME_URL; ?>/assets/dist/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= THEME_URL; ?>/assets/dist/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= THEME_URL; ?>/assets/dist/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="<?= THEME_URL; ?>/assets/dist/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?= THEME_URL; ?>/assets/dist/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="<?= THEME_URL; ?>/assets/dist/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <?php the_field('head_scripts', 'options'); ?>

    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>

    <script>
        window.Theme = <?php echo json_encode([
            'nonce' => wp_create_nonce(),
            'site_url' => get_home_url(),
            'theme_dir' => get_template_directory_uri(),
        ]); ?>
    </script>
</head>

<body <?php body_class(); ?>>

    <?php the_field('after_body_open_scripts', 'options'); ?>

    <div id="theme" v-cloak>
        <header id="site-header">
            <?php get_template_part('templates/layout/top-bar'); ?>
            <?php get_template_part('templates/layout/mobile-nav'); ?>
            <?php get_template_part('templates/layout/main-nav'); ?>
        </header>
