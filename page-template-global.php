<?php
use Cn\Theme;

/**
 * Template Name: Global Page
 *
 * This page handles the global content and page redirection
**/

$found_page = null;
$current_region = null;

// Allow the user to specify a ?location=<country code> url parameter to force a location
$force_location = $_GET['location'];
if ($force_location == null) {
    // If no location was specified, get the current region
    $current_region = Theme::get_current_region();
} else {
    // If a location was forced, get that region
    $current_region = Theme::get_region($force_location);
}

if ($current_region != null) {
    $redirect_pages = get_field('redirect_pages');
    foreach ($redirect_pages as $relationship) {
        // Check if the current region matches a redirect page, and if that redirect page has the autoredirect flag set
        if ($relationship['region']->ID == $current_region->ID && $relationship['enable_autoredirect'] === true) {
            $found_page = $relationship['page'];
        }
    }
}

if ($found_page) {
    header("HTTP/1.1 303 See Other");
    header("Location: {$found_page}");
} else {
    include('page.php');
}