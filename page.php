<?php 

get_header(); 

loop_posts(function() {
    render('page-content');
});

get_footer();