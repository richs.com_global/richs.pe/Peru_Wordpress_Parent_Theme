<?php 
/**
 * Template Name: Search
 */
get_header(); 

$region = Cn\Theme::get_current_region();

$replacedSearchParam = str_replace(array("{", "}"), "", htmlspecialchars($_GET['s'], ENT_QUOTES | ENT_HTML5, 'UTF-8'));
?>

<section class="block hero search">
    <div class="background" style="background-image:url(<?= THEME_URL . '/assets/dist/images/richs-gradient-bg.jpg'; ?>);">
        <div class="overlay gradient"></div>
        <div class="container">
            <div class="flex">
                <div class="content">
                    <h1 class="title">Search</h1>
                    <?php if (!empty($replacedSearchParam)) : ?>
                        <div class="copy">Results for "<?= $replacedSearchParam; ?>"</div>
                    <?php endif; ?>
                </div>
                <div class="secondary">
                        <span class="block mt-5 product-site" style="display:none;"> Looking for a specific product? Visit the 
                        <a class="hover:opacity-50 text-white underline " data-regional-marketing-link="true" href=""></a> 
                    product site</span>
                </div>
            </div>
        </div>
    </section>

<section class="container search-wrapper">
    <section class="content">
        <?php 
        if(have_posts()):
            loop_posts(function() {
                render('/pages/search/result');
            });
        ?>
        <div class="articles-nav">
            <?php the_posts_pagination( array(
                'mid_size' => 2,
                'prev_text' => __( '<span class="btn btn-red icon-btn reverse"><i class="fas fa-chevron-left"></i> Previous</span>', 'textdomain' ),
                'next_text' => __( '<span class="btn btn-red icon-btn">Next <i class="fas fa-chevron-right"></i></span>', 'textdomain' ),
            ) ); ?>
        </div>
        <?php else : ?>
            <h2 class='text-richsred text-center mb-0'>No results were found.</h2>
        <?php endif; ?>
    </section>
</section>
<?php
get_footer();