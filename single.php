<?php 

get_header(); 

$backgroundStyle = empty(get_the_post_thumbnail_url($post)) ? "" : "style=\"background-image:url(".get_the_post_thumbnail_url($post).")\"";
?>

<section class="block hero" <?= $backgroundStyle ?>>
    <div class="background">
        <div class="container">
            <div class="flex">
                <div class="content">
                    <a href="<?= get_permalink( get_option( 'page_for_posts' ) ); ?>"><h2 class="top-title"><i class="fas fa-chevron-left text-xs"></i> Back To Newsroom</h2></a>
                    <h1 class="title"><?= get_the_title(); ?></h1>
                </div>
                <div class="secondary">
                    <div class="text-with-border white">
                        <?= get_the_date("F j, Y"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container posts-wrapper">
    <section class="content article-content">
    <?php 
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post(); 
                the_content();
            }
        }
    ?>
    <span onclick="window.print();return false;" class="btn btn-red btn-icon cursor-pointer download">Download Article <i class="fas fa-chevron-right"></i></span>
    </section>
    <section class="sidebar">
        <div class="widget">
            <?php if ( is_active_sidebar( 'top_newsroom_sidebar' ) ) : ?>
                    <?php dynamic_sidebar( 'top_newsroom_sidebar' ); ?>
            <?php endif; ?>
        </div>
        <div class="link-list">
            <h4 class="widget-title">Explore By Category</h4>
            <ul>
                <?php wp_list_categories( array(
                'orderby'    => 'name',
                'title_li' => '',
                'show_count' => false,
                'exclude'    => array(  )
            ) ); ?>
            </ul>
        </div>

        <?php
        $args = array(
            'numberposts' => 10,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
        );
        $recent_posts = wp_get_recent_posts( $args );
        ?>
        <div class="list reverse">
            <h4 class="widget-title">Most Recent Stories</h4>
            <ul class="recent-stories link-list">
                <?php foreach($recent_posts as $recent_post): ?>
                <li>
                    <a href="<?= get_the_permalink($recent_post['ID']) ?>"><?= $recent_post['post_title'];?></a>
                </li> 
                <?php endforeach; ?>
            </ul> 
        </div>
        <div class="media-resources">
            <h4 class="widget-title">Important Media Resources</h4>
            <?php if ( is_active_sidebar( 'bottom_newsroom_sidebar' ) ) : ?>
                    <?php dynamic_sidebar( 'bottom_newsroom_sidebar' ); ?>
            <?php endif; ?>
        </div>
        </aside>
    </section>
    <?php
get_footer();