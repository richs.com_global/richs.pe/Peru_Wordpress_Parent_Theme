<?php

namespace Cn;

class Acf
{
    use Singleton;

    /**
     * Field group classes will automatially be registered
     */
    protected $field_groups = [
        // Acf\ExampleGroup::class,
        Acf\ThemeSettings\General::class,
        Acf\ThemeSettings\DesignOptions::class,
        Acf\ThemeSettings\ScriptsStyles::class,
        Acf\ThemeSettings\SocialMedia::class,
        Acf\ThemeSettings\EmployeeCta::class,
        Acf\CustomPostTypes\Regions::class,
        Acf\CustomPostTypes\Leaders::class,
        Acf\PageFields\RedirectionTemplateFields::class,
        Acf\PageFields\MicrositeTemplateFields::class,
        Acf\Blocks\TestimonialBanner::class,
        Acf\Blocks\MessageBanner::class,
        Acf\Blocks\Hero::class,
        Acf\Blocks\ContentCardCarousel::class,
        Acf\Blocks\TextWithImage::class,
        Acf\Blocks\Banner::class,
        Acf\Blocks\BrandCarousel::class,
        Acf\Blocks\LinkCards::class,
        Acf\Blocks\Video::class,
        Acf\Blocks\MarketoForm::class,
        Acf\Blocks\SearchBar::class,
        Acf\Blocks\ProductCategoryGrid::class,
        Acf\Blocks\TabbedForm::class,
        Acf\Blocks\SeeAllDestinations::class,
        Acf\Blocks\Map::class,
        Acf\Blocks\Highlights::class,
        Acf\Blocks\TextWithLinks::class,
        Acf\Blocks\CallToAction::class,
        Acf\Blocks\LeadershipGrid::class,
        Acf\Blocks\RegionSelector::class,
        Acf\Blocks\Wysiwyg::class,
        Acf\Blocks\ImageGrid::class,
        Acf\Blocks\ImageCarousel::class,
        Acf\Blocks\Pillars::class,
        Acf\Blocks\ProductCta::class,
        Acf\Blocks\ProductList::class,
        Acf\Blocks\Stats::class,
        Acf\Blocks\SimpleModal::class,
        Acf\Blocks\TextWithMap::class,
        Acf\Blocks\TextWithVideo::class,
        Acf\Blocks\Timeline::class,
        Acf\Blocks\GravityForm::class,
        Acf\PageFields\ProductFields::class,
        Acf\PageFields\PageFields::class
    ];

    public function __construct()
    {
        $this->register_options_page();
        $this->register_field_groups();
        $this->register_google_map_api_key();
        $this->register_filters();
    }

    /**
     * Register theme setting page
     */
    protected function register_options_page()
    {
        if (is_admin() && function_exists('acf_add_options_page')) {
            $option_page = [
                'page_title' => 'Theme Settings',
                'menu_title' => 'Theme',
                'menu_slug' => 'theme-settings',
                'capability' => 'edit_posts',
                'parent' => 'options-general.php',
                'redirect' => false
            ];
            acf_add_options_page($option_page);
        }
    }

    protected function register_field_groups()
    {
        foreach ($this->field_groups as $field_group) {
            (new $field_group)->register();
        }
    }

    protected function register_google_map_api_key()
    {
        if(defined('GOOGLE_API_KEY')) {
            add_action('acf/init', function() {
                acf_update_setting('google_api_key', GOOGLE_API_KEY);
            });
        }
    }

    protected function register_filters()
    {
        add_filter('acf/fields/post_object/query/name=global_page', [$this, 'filter_global_pages'], 10, 3);
    }

    public function filter_global_pages($args, $field, $post_id)
    {
        
        $args['meta_query'] = [
            'relation' => "AND",
            [
                'key' => '_wp_page_template',
                'value' => 'page-template-global.php',
                'compare' => "="
            ]
        ];
        return $args;
    }

}
