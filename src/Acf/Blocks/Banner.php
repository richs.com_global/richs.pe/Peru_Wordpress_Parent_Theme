<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Banner extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/banner')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('banner');
        $fields
            ->addRepeater('slides',['layout' => 'row'])
                ->addText('title')
                ->addTextArea('text')
                ->addText('name')
                ->addText('occupation')
                ->addFields($this->getCountryCodeSelect())
                ->addImage('background_image')
                ->addImage('recipe_image')
                ->addLink('button',['label' => 'Recipe Button'])
                ->addRadio('text_side', ['label' => 'Text Side', 'choices' => ['left', 'right']])
                ->addFields($this->getBackgroundOverlay())
                ->addTrueFalse('increase_text_contrast', ['ui' => 1])->setInstructions("Increases gradient opacity and direction to provide higher contrast between text and background")
            ->endRepeater()
            ->addTrueFalse('adjust_height', ['label' => 'Adjust Banner Height'])
            ->addText('banner_height')->conditional('adjust_height', '==', 1);
        return $fields;
    }
}