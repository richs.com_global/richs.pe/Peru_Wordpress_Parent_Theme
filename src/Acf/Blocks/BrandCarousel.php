<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class BrandCarousel extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/brand-carousel')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('brand-carousel');
        $fields
            ->addText('title')
            ->addRepeater('brands')
                ->addImage('logo')
                ->addImage('hover_image')
                ->addTextarea('copy')
                ->addLink('button')
            ->endRepeater();
        return $fields;
    }
}