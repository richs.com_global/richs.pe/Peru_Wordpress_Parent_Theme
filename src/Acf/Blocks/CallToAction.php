<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class CallToAction extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/call-to-action')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('call-to-action');
        $fields
            ->addText('title')
            ->addTextarea('text')
            ->addLink('button')
            ->addFields($this->getBackgroundColorSelect());
        return $fields;
    }
}