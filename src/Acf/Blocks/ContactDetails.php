<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ContactDetails extends FieldGroup

{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/contact-details')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('contact-details');
        $fields->addText('title')
            ->addRepeater('links')
                ->addImage('image')
                ->addLink('link')
            ->endRepeater()
            ->addWysiwyg('text')
            ->addLink('button');
        return $fields;
    }
}