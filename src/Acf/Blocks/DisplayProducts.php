<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class DisplayProducts extends FieldGroup

{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/display-products')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('display-products');
        $fields->addText('title');
        return $fields;
    }
}