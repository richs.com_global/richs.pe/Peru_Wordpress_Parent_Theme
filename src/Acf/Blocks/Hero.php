<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;

class Hero extends FieldGroup
{
    use ReusableFields;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/hero')
             ->addFields($this->getHeroFields())
             ->addFields($this->getBackgroundOverlay());
    }
}