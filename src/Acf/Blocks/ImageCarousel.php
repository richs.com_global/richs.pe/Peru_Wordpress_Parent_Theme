<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ImageCarousel extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/image-carousel')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('image-carousel');
        $fields
            ->addText('title')
            ->addWysiwyg('copy')
            ->addRepeater('images')
                ->addImage('image')
            ->endRepeater();
        return $fields;
    }
}