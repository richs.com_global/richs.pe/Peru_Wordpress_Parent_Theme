<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class LeadershipGrid extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/leadership-grid')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('highlights');
        $fields
            ->addText('title')
            ->addRelationship('people', ['post_type' => 'leader']);
        return $fields;
    }
}