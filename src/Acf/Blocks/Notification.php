<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;

class Notification extends FieldGroup
{
    use ReusableFields;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/notification')
             ->addFields($this->getNotificationFields());
    }
}