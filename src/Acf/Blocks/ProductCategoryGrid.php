<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class ProductCategoryGrid extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/product-category-grid')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('product-category-grid');
        $fields
            ->addText("title")
            ->addFields($this->getCountryCodeSelect())
            ->addRepeater('categories', ['layout' => 'block'])
                ->addTrueFalse("hide_header", ['ui' => 1, 'label' => 'Hide Card Header'])->setInstructions("Check this box to remove the card header from this card only.")
                ->addText("title")
                ->addTextarea("text")
                ->addLink("link")
                ->addFields($this->getCardButtons())
                ->addImage('background_image')
                ->addFields($this->getBackgroundOverlay())
            ->endRepeater();
        return $fields;
    }
}