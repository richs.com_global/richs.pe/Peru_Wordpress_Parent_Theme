<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class RegionSelector extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/region-selector')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('region_selector');
        $fields->addText('title');
        $fields->addText('secondary');
        $fields->addFields($this->getGlobalPageSelector());
        $fields->addFields($this->getRegionSelector('Current Region', 'current_region'));
        return $fields;
    }
}