<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class SimpleModal extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/simple-modal')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('simple-modal');
        $fields
            ->addTab('content_settings')
            ->addText('modal_id', ['label' => 'Modal ID', 'instructions' => 'Set this to match the anchor tag you will use to trigger the popup'])
            ->addWysiwyg('content')
            ->addTab('html_options')
                ->addFields($this->getHtmlAttributesField());
        return $fields;
    }
}