<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TestimonialCarousel extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/testimonial-carousel')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('testimonial-carousel');
        $fields
            ->addImage('background_image')
            ->addFields($this->getBackgroundOverlay())
            ->addText('title')
            ->addTextarea('suporting_text')
            ->addRepeater('brands')
                ->addImage('photo')
                ->addTextarea('copy')
                ->addText('name')
                ->addText('date')
            ->endRepeater();
        return $fields;
    }
}