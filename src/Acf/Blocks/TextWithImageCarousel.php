<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TextWithImageCarousel extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/text-with-image-carousel')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('text-with-image-carousel');
        $fields
            ->addText('title')
            ->addRepeater('slides')
                ->addText('title')
                ->addWysiwyg('text')
                ->addLink('button')
                ->addImage('image')
                ->addSelect('image_position', ['default_value' => "left", 'choices' =>
                [
                    ['left' => 'Left'],
                    ['right' => 'Right']
                ]])
            ->endRepeater();
            
        return $fields;
    }
}