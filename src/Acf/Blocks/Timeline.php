<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Timeline extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/timeline')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('timeline');
        $fields
            ->addTextarea("google-sheets-url", ['title' => "Google Sheets URL"]);
            
        return $fields;
    }
}