<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Video extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/video')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('video');
        $fields
            ->addText('title')
            ->addText('youtube_id')
            ->addImage('placeholder_image')
            ->addFields($this->getBackgroundOverlay());
        return $fields;
    }
}