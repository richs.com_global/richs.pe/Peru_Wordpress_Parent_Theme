<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;

class WorldMap extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/world-map')
            ->addImage('background_image')
            ->addLink("button");
    }
}