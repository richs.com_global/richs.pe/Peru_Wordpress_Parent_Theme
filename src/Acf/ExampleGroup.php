<?php

namespace Cn\Acf;

class ExampleGroup extends FieldGroup
{
    /**
     * Override the class properties in the parent FieldGroup to customize
     * the field group behavior.
     */
    protected $menu_order = 1;

    protected $style = 'seamless';

    protected $label_placement = 'below';

    /**
     * Define fields in this class.
     *
     * $this->builder is an instance of StoutLogic\AcfBuilder\FieldsBuilder
     * Function calls are automatically forwarded to the builder instance.
     *
     * E.g. $this->builder->addText() === $this->addText()
     */
    protected function build()
    {
        $this->setLocation('block', '==', 'my-block');

        $this->addText('label')
                ->setInstructions('Field instructions here.');
    }

}
