<?php

namespace Cn\Acf;

use Illuminate\Support\Str;
use StoutLogic\AcfBuilder\FieldsBuilder;

trait ReusableFields
{
    protected function getHtmlAttributesField(string $field_name = 'html_attributes', array $args = [])
    {
        $field = new FieldsBuilder($field_name);

        $args += ['layout' => 'table'];

        $field->addGroup($field_name, $args)
                ->addText('id', ['label' => 'ID'])
                ->addText('class');

        return $field;
    }

    protected function getHeroFields()
    {
        $fields = new FieldsBuilder('hero');
        $fields
            ->addText('top_title', ['label' => 'Top Title', 'instructions' => "Small title that appears above the title"])
            ->addSelect("hero_style", ['default_value' => "default", 'choices' =>
            [
                ['default' => "Default"],
                ['quote' => "Quote"]
            ]])
            ->addText('title')
            ->addTextarea('copy', ['instructions' => "(optional) Text to appear below the title"])
                ->conditional('hero_style', "==", 'default')
            ->addText('attribution')
                ->conditional('hero_style', "==", 'quote')
            ->addImage('background_image')
            ->addTrueFalse('is_add_announcement', ['label' => "Add Announcement?", 'message' => 'Add announcement'])
                ->setInstructions("Check to add an announcement below the hero image")
            ->addGroup('announcement')
                ->conditional('is_add_announcement', '==', '1')
                ->addText('text')
                ->addLink('button')
            ->endGroup();

        return $fields;
    }

    protected function getNotificationFields()
    {
        $fields = new FieldsBuilder('hero');
        $fields
            ->addTrueFalse('is_add_announcement', ['label' => "Add Announcement?", 'message' => 'Add announcement'])
            ->addSelect('background-color', ['label'=>"Background Color", 'default_value' => "white", 'choices' =>
        [
            ['white' => "White"],
            ['richsred' => "Red"],
            ['richs-gradient-45' => "Rich Red Gradient (45 degrees)"],
            ['richs-gradient-90' => "Rich Red Gradient (90 degrees)"],
            ['deep-red-gradient-90' => "Deep Red Gradient (90 degrees)"],
            ['gradient-with-text' => "Gradient with Text"]
        ]])
            ->addGroup('announcement')
                ->conditional('is_add_announcement', '==', '1')
                ->addText('title')
                ->addText('text')
                ->addLink('button')
            ->endGroup();

        return $fields;
    }

    protected function getBlockPaddingSelect()
    {
        $fields = new FieldsBuilder('block-padding');
        $fields->addSelect('block_padding', ['default_value' => "default-padding", 'choices' =>
        [
            ['default-padding' => "Default"],
            ['no-padding' => "No Padding"],
            ['reduced-padding' => "Reduced Padding"]
        ]]);
        return $fields;
    }

    protected function getBackgroundColorSelect()
    {
        $fields = new FieldsBuilder('background-color');
        $fields->addSelect('background-color', ['label'=>"Background Color", 'default_value' => "white", 'choices' =>
        [
            ['white' => "White"],
            ['richsred' => "Red"],
            ['richs-gradient-45' => "Rich Red Gradient (45 degrees)"],
            ['richs-gradient-90' => "Rich Red Gradient (90 degrees)"],
            ['deep-red-gradient-90' => "Deep Red Gradient (90 degrees)"],
            ['gradient-with-text' => "Gradient with Text"]
        ]]);
        return $fields;
    }

    protected function getCardButtons() {
        $fields = new FieldsBuilder('card_buttons');
        $fields->addLink('card_button_1')
            ->addLink('card_button_2');
            return $fields;
    }

    protected function getCardSizeSelect()
    {
        $fields = new FieldsBuilder('card_size');
        $fields->addSelect('card_size', ['default_value' => "size-small",
            'choices' => [
                ['size-small' => "Small (4 items)"],
                ['size-medium' => "Medium (3 items)"],
                ['size-large' => "Large (2 items)"],
                ['size-full' => "Full (1 item)"],
            ]
        ]);
        return $fields;
    }

    protected function getRegionSelector($title = null, $key = "region") {
        $fields = new FieldsBuilder('region_selector');
        $selector = $fields->addPostObject($key, ['post_type' => ['region'], 'allow_archives' => 0]);
        if ($title) {
            $selector->setConfig('label', $title);
        }
        return $fields;
    }

    protected function getGlobalPageSelector() {
        $fields = new FieldsBuilder('global-page');
        $fields->addPostObject('global_page', ['post_type' => ['page'], 'allow_archives' => 0]);
        return $fields;
    }

    protected function getBackgroundOverlay() {
        $fields = new FieldsBuilder('background-overlay');
        $fields->addTrueFalse('add_background_overlay')
            ->setInstructions("Check to add an overlay to darken the background image");
        $fields->addNumber('background_overlay_opacity', ['default_value'=>"0.35","min"=> ".1", "max" => ".5", "step" => ".05"])
            ->conditional('add_background_overlay', "==", 1);
        return $fields;
    }

    protected function getFontSelector() {
        $fields = new FieldsBuilder('font-selector');
        $fields->addSelect('font_family', ['default_value' => "alrightsans", 'choices' =>
        [
            ['alrightsans' => "Alright Sans"],
            ['drina' => "Drina"],
            ['marketpro' => "FF Market Pro"]
        ]]);
        $fields->addText('font_size', ['default_value' =>'36px']);
        return $fields;
    }

    protected function getAdminBlockHeader(string $name = '')
    {
        $name = $name ? $name : ucwords(str_replace('_', ' ', (Str::snake(class_basename($this)))));

        $adminBlockHeader = new FieldsBuilder('admin_block_header');

        $adminBlockHeader->addMessage($name, [
            'message' => '',
            'wrapper' => [
                'class' => 'admin-block-header',
            ]
        ]);

        return $adminBlockHeader;
    }



    protected function getCountryCodeSelect() {
        $countries = [
            [null => "No selection"],
            ["af" => "Afghanistan"],
            ["ax" => "Åland Islands"],
            ["al" => "Albania"],
            ["dz" => "Algeria"],
            ["as" => "American Samoa"],
            ["ad" => "Andorra"],
            ["ao" => "Angola"],
            ["ai" => "Anguilla"],
            ["aq" => "Antarctica"],
            ["ag" => "Antigua and Barbuda"],
            ["ar" => "Argentina"],
            ["am" => "Armenia"],
            ["aw" => "Aruba"],
            ["au" => "Australia"],
            ["at" => "Austria"],
            ["az" => "Azerbaijan"],
            ["bs" => "Bahamas"],
            ["bh" => "Bahrain"],
            ["bd" => "Bangladesh"],
            ["bb" => "Barbados"],
            ["by" => "Belarus"],
            ["be" => "Belgium"],
            ["bz" => "Belize"],
            ["bj" => "Benin"],
            ["bm" => "Bermuda"],
            ["bt" => "Bhutan"],
            ["bo" => "Bolivia"],
            ["ba" => "Bosnia and Herzegovina"],
            ["bw" => "Botswana"],
            ["bv" => "Bouvet Island"],
            ["br" => "Brazil"],
            ["io" => "British Indian Ocean Territory"],
            ["bn" => "Brunei Darussalam"],
            ["bg" => "Bulgaria"],
            ["bf" => "Burkina Faso"],
            ["bi" => "Burundi"],
            ["kh" => "Cambodia"],
            ["cm" => "Cameroon"],
            ["ca" => "Canada"],
            ["cv" => "Cape Verde"],
            ["ky" => "Cayman Islands"],
            ["cf" => "Central African Republic"],
            ["td" => "Chad"],
            ["cl" => "Chile"],
            ["cn" => "China"],
            ["cx" => "Christmas Island"],
            ["cc" => "Cocos (Keeling) Islands"],
            ["co" => "Colombia"],
            ["km" => "Comoros"],
            ["cg" => "Congo"],
            ["cd" => "Congo, The Democratic Republic of The"],
            ["ck" => "Cook Islands"],
            ["cr" => "Costa Rica"],
            ["ci" => "Cote D'ivoire"],
            ["hr" => "Croatia"],
            ["cu" => "Cuba"],
            ["cy" => "Cyprus"],
            ["cz" => "Czech Republic"],
            ["dk" => "Denmark"],
            ["dj" => "Djibouti"],
            ["dm" => "Dominica"],
            ["do" => "Dominican Republic"],
            ["ec" => "Ecuador"],
            ["eg" => "Egypt"],
            ["sv" => "El Salvador"],
            ["gq" => "Equatorial Guinea"],
            ["er" => "Eritrea"],
            ["ee" => "Estonia"],
            ["et" => "Ethiopia"],
            ["fk" => "Falkland Islands (Malvinas)"],
            ["fo" => "Faroe Islands"],
            ["fj" => "Fiji"],
            ["fi" => "Finland"],
            ["fr" => "France"],
            ["gf" => "French Guiana"],
            ["pf" => "French Polynesia"],
            ["tf" => "French Southern Territories"],
            ["ga" => "Gabon"],
            ["gm" => "Gambia"],
            ["ge" => "Georgia"],
            ["de" => "Germany"],
            ["gh" => "Ghana"],
            ["gi" => "Gibraltar"],
            ["gr" => "Greece"],
            ["gl" => "Greenland"],
            ["gd" => "Grenada"],
            ["gp" => "Guadeloupe"],
            ["gu" => "Guam"],
            ["gt" => "Guatemala"],
            ["gg" => "Guernsey"],
            ["gn" => "Guinea"],
            ["gw" => "Guinea-bissau"],
            ["gy" => "Guyana"],
            ["ht" => "Haiti"],
            ["hm" => "Heard Island and Mcdonald Islands"],
            ["va" => "Holy See (Vatican City State)"],
            ["hn" => "Honduras"],
            ["hk" => "Hong Kong"],
            ["hu" => "Hungary"],
            ["is" => "Iceland"],
            ["in" => "India"],
            ["id" => "Indonesia"],
            ["ir" => "Iran, Islamic Republic of"],
            ["iq" => "Iraq"],
            ["ie" => "Ireland"],
            ["im" => "Isle of Man"],
            ["il" => "Israel"],
            ["it" => "Italy"],
            ["jm" => "Jamaica"],
            ["jp" => "Japan"],
            ["je" => "Jersey"],
            ["jo" => "Jordan"],
            ["kz" => "Kazakhstan"],
            ["ke" => "Kenya"],
            ["ki" => "Kiribati"],
            ["kp" => "Korea, Democratic People's Republic of"],
            ["kr" => "Korea, Republic of"],
            ["kw" => "Kuwait"],
            ["kg" => "Kyrgyzstan"],
            ["la" => "Lao People's Democratic Republic"],
            ["lv" => "Latvia"],
            ["lb" => "Lebanon"],
            ["ls" => "Lesotho"],
            ["lr" => "Liberia"],
            ["ly" => "Libyan Arab Jamahiriya"],
            ["li" => "Liechtenstein"],
            ["lt" => "Lithuania"],
            ["lu" => "Luxembourg"],
            ["mo" => "Macao"],
            ["mk" => "Macedonia, The Former Yugoslav Republic of"],
            ["mg" => "Madagascar"],
            ["mw" => "Malawi"],
            ["my" => "Malaysia"],
            ["mv" => "Maldives"],
            ["ml" => "Mali"],
            ["mt" => "Malta"],
            ["mh" => "Marshall Islands"],
            ["mq" => "Martinique"],
            ["mr" => "Mauritania"],
            ["mu" => "Mauritius"],
            ["yt" => "Mayotte"],
            ["mx" => "Mexico"],
            ["fm" => "Micronesia, Federated States of"],
            ["md" => "Moldova, Republic of"],
            ["mc" => "Monaco"],
            ["mn" => "Mongolia"],
            ["me" => "Montenegro"],
            ["ms" => "Montserrat"],
            ["ma" => "Morocco"],
            ["mz" => "Mozambique"],
            ["mm" => "Myanmar"],
            ["na" => "Namibia"],
            ["nr" => "Nauru"],
            ["np" => "Nepal"],
            ["nl" => "Netherlands"],
            ["an" => "Netherlands Antilles"],
            ["nc" => "New Caledonia"],
            ["nz" => "New Zealand"],
            ["ni" => "Nicaragua"],
            ["ne" => "Niger"],
            ["ng" => "Nigeria"],
            ["nu" => "Niue"],
            ["nf" => "Norfolk Island"],
            ["mp" => "Northern Mariana Islands"],
            ["no" => "Norway"],
            ["om" => "Oman"],
            ["pk" => "Pakistan"],
            ["pw" => "Palau"],
            ["ps" => "Palestinian Territory, Occupied"],
            ["pa" => "Panama"],
            ["pg" => "Papua New Guinea"],
            ["py" => "Paraguay"],
            ["pe" => "Peru"],
            ["ph" => "Philippines"],
            ["pn" => "Pitcairn"],
            ["pl" => "Poland"],
            ["pt" => "Portugal"],
            ["pr" => "Puerto Rico"],
            ["qa" => "Qatar"],
            ["re" => "Reunion"],
            ["ro" => "Romania"],
            ["ru" => "Russian Federation"],
            ["rw" => "Rwanda"],
            ["sh" => "Saint Helena"],
            ["kn" => "Saint Kitts and Nevis"],
            ["lc" => "Saint Lucia"],
            ["pm" => "Saint Pierre and Miquelon"],
            ["vc" => "Saint Vincent and The Grenadines"],
            ["ws" => "Samoa"],
            ["sm" => "San Marino"],
            ["st" => "Sao Tome and Principe"],
            ["sa" => "Saudi Arabia"],
            ["sn" => "Senegal"],
            ["rs" => "Serbia"],
            ["sc" => "Seychelles"],
            ["sl" => "Sierra Leone"],
            ["sg" => "Singapore"],
            ["sk" => "Slovakia"],
            ["si" => "Slovenia"],
            ["sb" => "Solomon Islands"],
            ["so" => "Somalia"],
            ["za" => "South Africa"],
            ["gs" => "South Georgia and The South Sandwich Islands"],
            ["es" => "Spain"],
            ["lk" => "Sri Lanka"],
            ["sd" => "Sudan"],
            ["sr" => "Suriname"],
            ["sj" => "Svalbard and Jan Mayen"],
            ["sz" => "Swaziland"],
            ["se" => "Sweden"],
            ["ch" => "Switzerland"],
            ["sy" => "Syrian Arab Republic"],
            ["tw" => "Taiwan, Province of China"],
            ["tj" => "Tajikistan"],
            ["tz" => "Tanzania, United Republic of"],
            ["th" => "Thailand"],
            ["tl" => "Timor-leste"],
            ["tg" => "Togo"],
            ["tk" => "Tokelau"],
            ["to" => "Tonga"],
            ["tt" => "Trinidad and Tobago"],
            ["tn" => "Tunisia"],
            ["tr" => "Turkey"],
            ["tm" => "Turkmenistan"],
            ["tc" => "Turks and Caicos Islands"],
            ["tv" => "Tuvalu"],
            ["ug" => "Uganda"],
            ["ua" => "Ukraine"],
            ["ae" => "United Arab Emirates"],
            ["gb" => "United Kingdom"],
            ["us" => "United States"],
            ["um" => "United States Minor Outlying Islands"],
            ["uy" => "Uruguay"],
            ["uz" => "Uzbekistan"],
            ["vu" => "Vanuatu"],
            ["ve" => "Venezuela"],
            ["vn" => "Vietnam"],
            ["vg" => "Virgin Islands, British"],
            ["vi" => "Virgin Islands, U.S."],
            ["wf" => "Wallis and Futuna"],
            ["eh" => "Western Sahara"],
            ["ye" => "Yemen"],
            ["zm" => "Zambia"],
            ["zw" => "Zimbabwe"]];

            $fields = new FieldsBuilder('country');
            $fields->addSelect('country_code', ['choices' => $countries]);
            return $fields;
    }
}