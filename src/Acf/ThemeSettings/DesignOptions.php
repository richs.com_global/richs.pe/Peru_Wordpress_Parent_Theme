<?php

namespace Cn\Acf\ThemeSettings;

class DesignOptions extends \Cn\Acf\FieldGroup
{
    protected $menu_order = 101;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('options_page', '==', 'theme-settings');

        $this->addImage('logo');
        
        $this->addImage('mobile_logo');

        $this->addImage('footer_logo');

        $this->addImage('404_image');

        $this->addText('copyright_notice', [
            'instructions' => '"&copy; ' . date('Y') . '" is pre populated in the footer template',
        ]);

        $this->addImage('footer_background');
    }
}
