<?php

namespace Cn\Acf\ThemeSettings;

class EmployeeCta extends \Cn\Acf\FieldGroup
{
    protected $menu_order = 130;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('options_page', '==', 'theme-settings');

        $this->addTrueFalse('enable_employee_cta');
        $this->addLink('employee_cta_button')
            ->conditional('enable_employee_cta', "==", '1');
    }
}
