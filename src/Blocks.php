<?php

namespace Cn;

class Blocks
{
    use Singleton;

    private $blocks;

    protected function __construct()
    {
        if($this->blocks = $this->get_blocks()) {
            $this->register_blocks();
        }
    }

    /**
     * Gets an array of blocks from the Blocks directory
     *
     * @return array $blocks
     */
    private function get_blocks()
    {
        return glob(dirname(__FILE__) . '/Blocks/*', GLOB_NOSORT);
    }

    /**
     * Registers all blocks
     *
     * @return void
     */
    private function register_blocks()
    {
        foreach($this->blocks as $block) {
            $file_name = pathinfo(basename($block), PATHINFO_FILENAME); // Get the filen ame without the .php extension. The file name will have the same name as the class.

            if($file_name === 'Block') continue; // Skip base block class

            $class = "Cn\Blocks\\{$file_name}";

            // If the class exists, create a new instance of it.
            if(class_exists($class)) {
               new $class();
            }
        }
    }
}