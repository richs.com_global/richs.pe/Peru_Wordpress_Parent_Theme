<?php

namespace Cn\Blocks;

/**
 * @package  ThemeName WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
abstract class Block
{
    private $name;

    /**
     * Registers a new Gutenberg block with ACFs
     *
     * @param array $args
     * @return void
     */
    protected function register_block(string $name = '', array $args = [])
    {
        $this->name = $name;

        $default_args = [
            'name'			  => strtolower($this->name),
            'title'			  => ucwords($this->name),
            'description'	  => 'A custom ' . strtolower($this->name) . ' block.',
            'render_callback' => [$this, 'render_callback'],
            'mode' => 'edit'
        ];

        $args = array_merge($default_args, $args);

        if(function_exists('acf_register_block')) {
            acf_register_block($args);
        }

        add_filter('allowed_block_types', [$this, 'add_block_to_allowed_blocks'], 11);
    }

    /**
     * Add blocks to allowed blocks
     *
     * @param Mixed $allowed_blocks
     * @return Array
     */
    public function add_block_to_allowed_blocks($allowed_blocks)
    {
        $allowed_blocks = $allowed_blocks ? $allowed_blocks : [];

        $allowed_blocks[] = 'acf/' . str_replace(' ', '-', trim(strtolower($this->name))); 

        return $allowed_blocks;
    }

    /**
     * Renders the block template
     *
     * @param array $block
     * @return void
     */
    public function render_callback($block)
    {
        $template_name = str_replace('acf/', '', $block['name']) . '.php';

        if($template = locate_template("templates/blocks/{$template_name}")) {
            $block = $this->get_block_data($block);
            //if (!$block) $block = $this->get_default_values();
            //include($template);
            $block_template = locate_template("templates/blocks/block-wrapper.php");
            include($block_template);
        }
    }

    public function render_from_code($block) {
        $block['block_name'] = $this->name;
        $template_name = str_replace('acf/', '', $block['name']) . '.php';
        
        if($template = locate_template("templates/blocks/{$template_name}")) {
            $block_template = locate_template("templates/blocks/block-wrapper.php");
            include($block_template);
        }
    }

    /**
     * Get the default values from the POST data.
     * We need this because gutenberg has issues displaying the default values of an ACF block
     * when it is first added to the gutenberg editor. This grabs the default values which are in
     * the POST request sent to Gutenberg when a block is added.
     */
    protected function get_default_values() {
        $values = json_decode(filter_input(INPUT_POST, "block"));

        $defaults = [];
        if ($values != null) {
            foreach ($values->data as $key => $value) {
                $defaults[$key] = $value;
            }
        }
        return $defaults;
    }

    protected function get_block_data($block) {
        $data = [];

        $block_name = str_replace('acf/', '', $block['name']);
        $data['block_name'] = $block_name;
        $data['template'] = $block_name . '.php';
        if ($fields = get_fields()) {
            $data += $fields;
        } else {
            $data += $this->get_default_values();
        }
        return $data;
    }

    public static function get_default_html_classes($block) {
        $html_classes = [];
        $html_classes[] = $block['block_name'];
        if ($padding_class = $block['block_padding']) {
            $html_classes[] = $padding_class;
        }

        if ($background_class = $block['background-color']) {
            $html_classes[] = "background-{$background_class}";
        }

        if ($extra_class = $block['html_attributes']['class']) {
            $html_classes[] = $extra_class;
        }

        return implode(" ", $html_classes);
    }
}