<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class BrandCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'brand-carousel',
            [
                'title'           => 'Brand Carousel',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['brand', 'brands', 'carousel']
            ]
        );
    }
}