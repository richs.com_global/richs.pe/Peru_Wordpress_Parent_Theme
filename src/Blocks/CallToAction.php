<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class CallToAction extends Block
{
    public function __construct()
    {
        parent::register_block(
            'call-to-action',
            [
                'title'           => 'Call to Action',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['callout']
            ]
        );
    }
}