<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */
// class TextWithLinks extends Block
class ContactDetails extends Block
{
    public function __construct()
    {
        parent::register_block(
            'contact-details',
            [
                'title'           => 'Contact Details',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'links']
            ]
        );
    }
}