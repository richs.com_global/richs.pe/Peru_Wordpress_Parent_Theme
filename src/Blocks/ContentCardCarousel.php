<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class ContentCardCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'content-card-carousel',
            [
                'title'           => 'Content Card Carousel',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['content-card-carousel']
            ]
        );
    }
}