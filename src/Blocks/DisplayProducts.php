<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   BIGBrave
 * @version  $Revision: 1.0.0
 */
// class TextWithLinks extends Block
class DisplayProducts extends Block
{
    public function __construct()
    {
        parent::register_block(
            'display-products',
            [
                'title'           => 'Display Products',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'links']
            ]
        );
    }
}