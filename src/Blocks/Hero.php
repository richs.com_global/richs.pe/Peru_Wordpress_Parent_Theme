<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Hero extends Block
{
    public function __construct()
    {
        parent::register_block(
            'hero',
            [
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['hero']
            ]
        );
    }
}