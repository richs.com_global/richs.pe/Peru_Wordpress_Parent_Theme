<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class ImageCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'image-carousel',
            [
                'title'           => 'Image Carousel',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['images']
            ]
        );
    }
}