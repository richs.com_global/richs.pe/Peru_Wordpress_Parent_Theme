<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class LeadershipGrid extends Block
{
    public function __construct()
    {
        parent::register_block(
            'leadership-grid',
            [
                'title'           => 'Leadership Grid',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['leaders', 'people', 'employees']
            ]
        );
    }
}