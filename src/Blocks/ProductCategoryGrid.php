<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class ProductCategoryGrid extends Block
{
    public function __construct()
    {
        parent::register_block(
            'product-category-grid',
            [
                'title'           => 'Product Category Grid',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['categories', 'product', 'products', 'grid']
            ]
        );
    }
}