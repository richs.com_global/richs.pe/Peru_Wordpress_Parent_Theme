<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class RegionSelector extends Block
{
    public function __construct()
    {
        parent::register_block(
            'region-selector',
            [
                'title'           => 'Region Selector',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['region', 'select region']
            ]
        );
    }
}