<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class SearchBar extends Block
{
    public function __construct()
    {
        parent::register_block(
            'search-bar',
            [
                'title'           => 'Search Bar',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['search', 'find']
            ]
        );
    }
}