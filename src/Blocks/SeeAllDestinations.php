<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class SeeAllDestinations extends Block
{
    public function __construct()
    {
        parent::register_block(
            'see-all-destinations',
            [
                'title'           => 'See All Destinations',
                'category'		  => 'layout',
                'icon'			  => 'admin-site-alt',
                'keywords'		  => ['destinations', 'dropdown', 'markets']
            ]
        );
    }
}