<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Stats extends Block
{
    public function __construct()
    {
        parent::register_block(
            'stats',
            [
                'title'           => 'Stats',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['stats', 'statistics']
            ]
        );
    }
}