<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */
class TestimonialCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'testimonial-carousel',
            [
                'title'           => 'Testimonial Carousel',
                'category'        => 'layout',
                'icon'            => 'admin-comments',
                'keywords'        => ['customer', 'testimonial', 'carousel']
            ]
        );
    }
}
