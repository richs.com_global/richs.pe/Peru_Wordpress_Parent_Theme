<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */
class TextWithImageCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-image-carousel',
            [
                'title'           => 'Text With Image Carousel',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['image']
            ]
        );
    }
}