<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class WorldMap extends Block
{
    public function __construct()
    {
        parent::register_block(
            'world-map',
            [
                'title'           => 'World Map',
                'category'		  => 'layout',
                'icon'			  => 'admin-site-alt',
                'keywords'		  => ['World Map', 'destinations', 'markets']
            ]
        );
    }
}