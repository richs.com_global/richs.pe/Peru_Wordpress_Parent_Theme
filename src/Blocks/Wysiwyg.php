<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Wysiwyg extends Block
{
    public function __construct()
    {
        parent::register_block(
            'wysiwyg',
            [
                'title'           => 'Wysiwyg Editor',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'content', 'editor']
            ]
        );
    }
}