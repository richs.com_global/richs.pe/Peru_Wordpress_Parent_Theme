<?php

namespace Cn;

use PostTypes\PostType;
use PostTypes\Taxonomy;

class PostTypes
{
    use Singleton;

    public function __construct()
    {
        $this->register_region_cpt();
        $this->register_person_cpt();
        //$this->register_my_taxonomy();
    }

    public function register_region_cpt()
    {
        (new PostType('region'))
                ->options([
                    'publicly_queryable' => false,
                    'exclude_from_search' => true
                ])
            ->icon('dashicons-admin-site-alt')
            ->register();
    }

    public function register_person_cpt()
    {
        $names = [
            'name'     => 'leader',
            'singular' => 'Leader',
            'plural'   => 'Leaders',
            'slug'     => 'leaders',
        ];
        (new PostType($names))
            ->icon('dashicons-groups')
            ->register();
    }
    
    public function register_my_taxonomy()
    {
        (new Taxonomy('my_taxonomy'))
            ->options([
                // options
            ])
            ->posttype('custom_post_type')
            ->register();
    }
}