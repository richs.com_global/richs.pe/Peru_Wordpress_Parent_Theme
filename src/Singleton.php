<?php

namespace Cn;

/**
 * Singleton Trait 
 * https://phptherightway.com/pages/Design-Patterns.html
 * https://stackoverflow.com/a/15870364
 */
trait Singleton
{
    private static $instances = [];

    /**
     * Protected constructor to prevent class from being initialized
     * Abstract contructor must be defined in the class
     */
    abstract protected function __construct();

    /**
     * Prevents cloning by declaring a protected, empty __clone() magic method
     *
     * @return void
     */
    protected function __clone()
    {
        // Intentionally empty
    }

    /**
     * Prevents cloning by unserialisation by declaring __wakeup() magic method that throws an exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }

    /**
     * Get singleton instance
     *
     * @return object Instance of class being inited
     */
    public static function init()
    {
        $class = get_called_class(); // late-static-bound class name

        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static;
        }

        return self::$instances[$class];
    }
}