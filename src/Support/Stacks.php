<?php

namespace Cn\Support;

class Stacks {

    protected static $stacks = [];

    public static function add($stack, $content) {
        self::$stacks[$stack][] = $content;
    }

    public static function get($stack) {
        return implode("\n", array_get(self::$stacks, $stack, [])) . "\n";
    }

}