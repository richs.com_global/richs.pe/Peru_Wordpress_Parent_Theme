<?php

use Cn\Support\Stacks;

/*
|--------------------------------------------------------------------------
| Wordpress template render helpers
|--------------------------------------------------------------------------
|
*/
if(! function_exists('render')) {
    /**
     * Render a template
     */
    function render($template, $display = true)
    {
        $template = trim($template, '/');

        if (!preg_match("/.php$/", $template)) {
            $template .= '.php';
        }

        $template = 'templates/' . $template;

        global $post;
        ob_start();
        include locate_template($template);
        $html = ob_get_clean();

        if (!$display) {
            return $html;
        }

        echo $html;
        return true;
    }
}

if (! function_exists('loop_posts')) {
    /**
     * Loop posts
     */
    function loop_posts($callback)
    {
        if (have_posts()) {
            while (have_posts()) {
                the_post();

                $callback();

            }
        }
    }
}

if(!function_exists('loop_field')) {
    /**
     * Loop repeater or group field
     */
    function loop_field($field_name, $template)
    {
        if(have_rows($field_name)) {
            while (have_rows($field_name)) {
                the_row();

                if (is_callable($template)) {
                    $template();

                } else {
                    render($template);
                }

            }
        }
    }
}

if(!function_exists('loop_layouts')) {
    /**
     * Loop layouts
     */
    function loop_layouts($field_name, $templates)
    {
        if(have_rows($field_name)) {
            while (have_rows($field_name)) {
                the_row();

                $template = array_get($templates, get_row_layout());

                if(is_callable($template)) {
                    $template();

                } elseif($template) {
                    render($template);
                }

            }
        }
    }
}


/*
|--------------------------------------------------------------------------
| Theme framework helpers
|--------------------------------------------------------------------------
|
*/
if(!function_exists('stack')) {
    /**
     * Stack helper
     *
     * @param  string  $stack
     * @param  string  $content
     */
    function stack($stack, $content = null)
    {
        if(is_null($content)) {
            return Stacks::get($stack);
        }

        Stacks::add($stack, $content);

        return true;
    }
}

if (!function_exists('bg_image')) {
    /**
     * Output a background image div.
     *
     * @param  string  $image
     * @param  string  $class
     * @param  array   $attributes
     * @return string
     */
    function bg_image($image, $class = '', array $attributes = [])
    {
        $attributes += [
            'class' => 'bg-image',
            'style' => ''
        ];

        $attributes['class'] .= " {$class}";
        $attributes['style'] .= "; background-image: url({$image});";

        $attributes = stringify_attributes($attributes);

        return "<div {$attributes}></div>";
    }
}

if (!function_exists('stringify_attributes')) {
    /**
     * Stringify attributes for use in HTML tags.
     *
     * Helper function used to convert a string, array, or object
     * of attributes to a string.
     *
     * @param  mixed $attributes
     * @return string
     */
    function stringify_attributes($attributes) {
        if (empty($attributes)) {
            return;
        }

        if (is_string($attributes)) {
            return ' ' . $attributes;
        }

        $attributes = array_map(function($value, $attribute) {
            if(is_int($attribute)) {
                return $value;
            }

            return sprintf('%s="%s"', $attribute, $value);
        }, $attributes, array_keys($attributes));

        return ' ' . implode($attributes, ' ');
    }
}

if (! function_exists('asset')) {
    /**
     * Generate an asset path for the theme.
     *
     * @param  string  $path
     * @return string
     */
    function asset($path)
    {
        return get_template_directory_uri() . '/assets/' . trim($path, '/');
    }
}


if (! function_exists('svg')) {
    /**
     * Generate an asset path for the theme.
     *
     * @param  string  $path
     * @return string
     */
    function svg($path, $class = '', $attrs = [])
    {
        $path = trim(parse_url($path, PHP_URL_PATH), '/');
        if (!preg_match("/.svg$/", $path)) {
            $path .= '.svg';
        }

        if (is_array($class)) {
            $attrs = $class;
            $class = '';
        }

        $attrs = array_merge([
            'class' => $class,
        ], $attrs);

        $svg = file_get_contents( ABSPATH . $path );

        return str_replace(
            '<svg',
            sprintf('<svg%s', stringify_attributes($attrs)),
            $svg
        );

    }
}

if (!function_exists('get_country_codes_array')) {

    function get_country_codes_array() {
        //https://github.com/umpirsky/country-list/tree/master/data/en
        $countries = array (
            'AF' => 'Afghanistan',
            'AX' => 'Åland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua & Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AC' => 'Ascension Island',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia & Herzegovina',
            'BW' => 'Botswana',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'IC' => 'Canary Islands',
            'CV' => 'Cape Verde',
            'BQ' => 'Caribbean Netherlands',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'EA' => 'Ceuta & Melilla',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo - Brazzaville',
            'CD' => 'Congo - Kinshasa',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Côte d’Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CW' => 'Curaçao',
            'CY' => 'Cyprus',
            'CZ' => 'Czechia',
            'DK' => 'Denmark',
            'DG' => 'Diego Garcia',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'SZ' => 'Eswatini',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong SAR China',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'XK' => 'Kosovo',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Laos',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao SAR China',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar (Burma)',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'KP' => 'North Korea',
            'MK' => 'North Macedonia',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territories',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn Islands',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'XA' => 'Pseudo-Accents',
            'XB' => 'Pseudo-Bidi',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Réunion',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'RW' => 'Rwanda',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'São Tomé & Príncipe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SX' => 'Sint Maarten',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia & South Sandwich Islands',
            'KR' => 'South Korea',
            'SS' => 'South Sudan',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'BL' => 'St. Barthélemy',
            'SH' => 'St. Helena',
            'KN' => 'St. Kitts & Nevis',
            'LC' => 'St. Lucia',
            'MF' => 'St. Martin',
            'PM' => 'St. Pierre & Miquelon',
            'VC' => 'St. Vincent & Grenadines',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard & Jan Mayen',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad & Tobago',
            'TA' => 'Tristan da Cunha',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks & Caicos Islands',
            'TV' => 'Tuvalu',
            'UM' => 'U.S. Outlying Islands',
            'VI' => 'U.S. Virgin Islands',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican City',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'WF' => 'Wallis & Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );

        return $countries;
    }
}

if (!function_exists('country_name_from_abbreviation')) {
    function country_name_from_abbreviation($abbreviation) {
        $abbreviation = strtoupper($abbreviation);
        $countries = get_country_codes_array();

        if (array_key_exists($abbreviation, $countries)) {
            return $countries[$abbreviation];
        } else {
            return null;
        }
    }
    
}