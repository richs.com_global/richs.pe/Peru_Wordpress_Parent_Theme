<div class="container">
        <div class="content-container row">
            <h2 class="block-title"><?= $block['title']; ?></h2>
        </div>
    <div class="contact-row row">
        <div class="links col-md-5">
            <div class="contact-tb">
             <?php foreach($block['links'] as $item) : $link = $item['link']; ?>
                <ul>
                    <li><img class="contact-tb" src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>" /></li>
                    <li><a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>"><?= $link['title']; ?></a></li>
                </ul>
            <?php endforeach; ?>
            </div>
             <div class="text contact-tb"><?= $block['text']; ?></div>
        </div>
        <div class="content-container col-md-7">
            <div class="text-container">
                <ul>
                    <li><img src="/wp-content/uploads/2020/01/location.svg" width="auto" height="20" class="alignnone size-small" /></li>
                    <li><?php if ($button = $block['button']) : ?><a class="contact-l" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a><?php endif; ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
