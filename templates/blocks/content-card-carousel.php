<?php if($block):
    $item_count = 3;
    switch ($block['card_size']) {
        case 'size-small':
            $item_count = 4;
            break;
        case 'size-medium':
            $item_count = 3;
            break;
        case 'size-large':
            $item_count = 2;
            break;
        case 'size-full':
            $item_count = 1;
            break;
    }

    $options = [
        'slidesToShow' => $item_count,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => true,
        'touchThreshold' => 100,
        'variableWidth' => $item_count > 1,
        'infinite' => false,
        'responsive' => [
            [
                'breakpoint' => 1234,
                'settings' => [
                    'slidesToShow' => 2,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 768,
                'settings' => [
                    'slidesToShow' => 2,
                    'arrows' => false,
                    'autoplay' => false,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 567,
                'settings' => [
                    'slidesToShow' => 1,
                    'arrows' => false,
                    'autoplay' => false,
                    'autoplaySpeed' => 3000,
                    'adaptiveHeight' => true
                ]
            ]
        ]
    ];
?>
    <div class="container">
        <?php if ($title = $block['title']) : ?>
            <h2 class="block-title"><?= $title; ?></h2>
        <?php endif; ?>
        <div class="carousel-container">
            <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel card-carousel item-count-<?= count($block['cards']); ?> card-<?= $block['card_size']; ?>">
                <?php foreach ($block['cards'] as $card) : 
                    $background_style = "";

                    $background_image = $card['background_image'];
                    if ($background_image != null) {
                        if($block['card_size'] == "size-large" || $block['card_size'] == "size-full") {
                            $background_style = "background-image: url({$background_image['url']});";
                        }
                        else {
                            $background_style = "background-image: url({$background_image['sizes']['card-square']});";
                        }
                    }

                    $header = $card['card_header'];
                ?>
                    <div class="card-wrapper">
                        <?php if ($card_link = $card['card_background_link']) : ?>
                            <a href="<?=$card_link['url']; ?>" target="<?= $card_link['target']; ?>">
                        <?php endif;?>
                        <div class="card <?= $block['card_size']; ?> <?php if(!$card['disable_hover']){echo 'hover-effect';}?>" style="<?= $style; ?>">
                            <div class="background" style="<?= $background_style; ?>"></div>
                            <?php if($card['use_background_video'] && $card['background_video']):?>
                                <video loop muted playsinline class="w-auto h-auto min-w-full min-h-full absolute" style="max-height:none;max-width:none; top:50%;left:50%;transform:translate(-50%,-50%);">
                                    <source src="<?= $card['background_video']['url'] ?>" type="<?= $card['background_video']['mime_type'] ?>">
                                </video>
                            <?php endif; ?>
                            <?php if ($background_style != "") :
                                $overlay_opacity = 0;
                                if ($card['add_background_overlay'] === true) {
                                    $overlay_opacity = $card['background_overlay_opacity'];
                                }
                            ?>
                                <div class="overlay" style="opacity: <?= $overlay_opacity; ?>;"></div>
                            <?php endif; ?>
                            
                            <?php if ($header['text']) : ?>
                                <div class="header">
                                    <?php if ($country_code = $block['country_code']) : ?>
                                        <span class="mr-1 flag-icon flag-icon-<?= $country_code; ?>"></span>
                                    <?php endif; ?>
                                    <?php if ($header['text']) : ?>
                                        <span class="text"><?= $header['text']; ?></span>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="card-content">
                                <div class="text-container">
                                    <?php if ($title = $card['card_title']) : ?>
                                        <h3 class="title"><?= $title; ?></h3>
                                    <?php endif; ?>
                                    <?php if ($content = $card['card_copy']) : ?>
                                        <div class="copy"><?= $content; ?></div>
                                    <?php endif; ?>
                                </div>
                                <?php if ($button = $card['card_button_1']) : ?>
                                    <div>
                                        <a class="btn icon-btn button-1" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-external-link-square-alt"></i></a>
                                    </div>
                                <?php endif; ?>
                                <?php if ($button = $card['card_button_2']) : ?>
                                    <div>
                                        <a class="btn icon-btn button-2" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-external-link-square-alt"></i></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if ($card_link = $card['card_background_link']) : ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </slick-carousel>
        </div>
    </div>
<?php endif; ?>