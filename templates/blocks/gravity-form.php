<div class="container">
    <div class="form-container <?= $block['style']; ?>" <?= $block['redirect_after_submission'] ? 'data-redirect="true" data-redirect-url="' . $block['redirect_url'] . '"' : "" ?>>
        <?php if($block['style'] == "two-columns") : ?>
            <div class="cn-col cn-col-1">
                <h2 class="block-title"><?= $block['title']; ?></h2>
                <p><?= $block['text']; ?></p>
            </div>
            <div class="cn-col cn-col-2">
                <?php gravity_form( $block['gravity_form_id'] , false, false, false, '', true ); ?>
            </div>
        <?php else: ?>

            <?php if ($title = $block['title']) : ?>
                <h2 class="block-title"><?= $title; ?></h2>
            <?php endif; ?>
            <?php if ($text = $block['text']) : ?>
                <p class="text"><?= $text; ?></p>
            <?php endif; ?>

                <?php gravity_form( $block['gravity_form_id'] , false, false, false, '', true ); ?>

        <?php endif; ?>
    </div>
</div>