<?php if($block):
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '<button type="button" class="slick-previous"><i class="fas fa-chevron-left"></i></button>',
        'nextArrow' => '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
        'speed' => 800,
        'dots' => true,
        //'autoplay' => true,
        //'autoplaySpeed' => 7000,
        'variableWidth' => true,
        'touchThreshold' => 100,
    ];
?>
    <div class="container">
        <?php if ($title = $block['title']) : ?>
            <h2 class="block-title"><?= $title; ?></h2>
        <?php endif; ?>
        <?php if ($copy = $block['copy']) : ?>
            <div class="copy"><?= $copy; ?></div>
        <?php endif; ?>
        <div class="carousel-container">
            <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel">
                <?php foreach ($block['images'] as $item) : 
                    $image = $item['image'];
                ?>
                    <img src="<?= $image['url']; ?>" />
                <?php endforeach; ?>
            </slick-carousel>
        </div>
    </div>
<?php endif; ?>