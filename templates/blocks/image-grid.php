<div class="container">
    <?php if ($title = $block['title']) : ?>
        <h2 class="block-title"><?= $title; ?></h2>
    <?php endif; ?>
    <div class="flex">
        <div class="cn-col cn-col-1">
            <img src="<?= $block['image_tall_vertical']['url']; ?>">
        </div>
        <div class="cn-col cn-col-2">
            <img src="<?= $block['image_small_1']['url']; ?>">
            <img src="<?= $block['image_small_2']['url']; ?>">
        </div>
        <div class="cn-col cn-col-3">
            <img src="<?= $block['image_large']['url']; ?>">
        </div>
    </div>
</div>