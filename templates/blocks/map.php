<?php 
use Cn\Theme;

if($block):
    
$posts = Theme::get_all_regions();

$postArr = array();
foreach($posts as $post){
    $postArr[get_the_title($post)]['links'] = get_field('marketing_sites', $post); 
    $postArr[get_the_title($post)]['country_codes'] = get_field('country_codes', $post);
    $postArr[get_the_title($post)]['flag'] = get_field('country_code', $post);
    $postArr[get_the_title($post)]['coming_soon'] = get_field('coming_soon', $post);
    $postArr[get_the_title($post)]['x_coordinate'] = get_field('x_coordinate', $post);
    $postArr[get_the_title($post)]['y_coordinate'] = get_field('y_coordinate', $post);
}


$current_region = Theme::get_current_region();


if ($current_region == null) {
    $current_region == "global";
}

?>
<div id="map-section" class="bg" style="background-image:url(<?= $block['background_image']['url']; ?>);">
    <div class="container">
        <div class="header">
            <h2 class="mb-1"><?= $block['title'] ?></h2>
            <div class="subtitle">
                <p><?= $block['sub-title'] ?></p>
                <button class="scroll-to-explore text-white">Hover over the map to explore</button>
            </div>
        </div>
        <world-map currentregion="<?= $current_region->post_title; ?>" json='<?php echo htmlentities(json_encode($postArr), ENT_QUOTES) ?>'>
            <?php echo file_get_contents(THEME_URL . "/templates/svg/world-map.svg"); ?>
            <div id="popup" style="display:none;">
                <div class="popup-header"><span class="data"><span class="flag-icon flag-icon-us"></span>US/Canada</span> 
                <div class="close"><img
                    src="data:image/svg+xml,%3Csvg%20enable-background%3D%22new%200%200%20256%20256%22%20height%3D%22256px%22%20version%3D%221.1%22%20fill%3D%22%23910b1c%22%20viewBox%3D%220%200%20256%20256%22%20width%3D%22256px%22%20xml%3Aspace%3D%22preserve%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%3Cpath%20d%3D%22M137.051%2C128l75.475-75.475c2.5-2.5%2C2.5-6.551%2C0-9.051s-6.551-2.5-9.051%2C0L128%2C118.949L52.525%2C43.475%20%20c-2.5-2.5-6.551-2.5-9.051%2C0s-2.5%2C6.551%2C0%2C9.051L118.949%2C128l-75.475%2C75.475c-2.5%2C2.5-2.5%2C6.551%2C0%2C9.051%20%20c1.25%2C1.25%2C2.888%2C1.875%2C4.525%2C1.875s3.275-0.625%2C4.525-1.875L128%2C137.051l75.475%2C75.475c1.25%2C1.25%2C2.888%2C1.875%2C4.525%2C1.875%20%20s3.275-0.625%2C4.525-1.875c2.5-2.5%2C2.5-6.551%2C0-9.051L137.051%2C128z%22%2F%3E%3C%2Fsvg%3E"
                    style="width: 100%; height:20px;" alt="Close"></div>
                </div>
                <div class="description">
                </div>
            </div>
        </world-map>
        <div class="see-all-destinations" style="display:none;">
            <destinations>
                <content-box title="Explore Our Regions">
                    <?php include 'all-destinations/pop-up.php'; ?>
                </content-box>
            </destinations>
        </div>
    </div>
</div>
<?php endif; ?>