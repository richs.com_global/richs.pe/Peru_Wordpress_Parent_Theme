<div class="container">
    <div class="form-container <?= $block['style']; ?>">
        <div class="cn-col cn-col-1">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <p><?= $block['text']; ?></p>
        </div>
        <div class="cn-col cn-col-2">
            <?php 
            //default the form ID to the customer care form
            if(!isset($block['marketo_id'])) {$block['marketo_id'] = "1044";} 
            ?>

            <marketo-form marketoid="<?= $block['marketo_id']; ?>"><form id="mktoForm_<?= $block['marketo_id']; ?>"></form></marketo-form>
        </div>
    </div>
</div>