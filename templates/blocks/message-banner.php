<?php if($block):
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => false,
    ];

    $slide_count = count($block['slides']);
    $show_timer = $slide_count > 1;
?>
    <div class="banner-carousel-container">
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php for($i = 0; $i < count($block['slides']); $i++) : $slide = $block['slides'][$i]; ?>
                <div class="background-wrapper" style="background-image:url(<?= $slide['background_image']['url']; ?>); min-height:<?= $slide['background_image']['height']; ?>px;">
                <?php
                    $overlay_opacity = 0;
                    if ($slide['add_background_overlay'] === true) {
                        $overlay_opacity = $slide['background_overlay_opacity'];
                    }
                    ?>
                    <div class="overlay gradient" style="opacity: <?= $overlay_opacity; ?>;"></div>
                    <div class="container">
                        <div class="content">
                            <h2 class="title font-<?= $slide['font_family'];?>" style="font-size:<?= $slide['font_size'];?>"><?= $slide['title']; ?></h2>
                            <div class="text"><?= $slide['content']; ?></div>

                            <?php if ($button = $slide['button']) : ?>
                                <a class="btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                            <?php if ($show_timer) : ?>
                                <div class="progress-timer"></div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </slick-carousel>
    </div>
<?php endif; ?>