<?php if ($block) : ?>

    <?php if ($block['is_add_announcement']) : ?>
        <div class="announcement">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-xs-12">
                        <div class="tile-text"><?= $block['announcement']['title']; ?></div>
                        <div class="text"><?= $block['announcement']['text']; ?></div>
                     </div>
                     <div class="col-md-2 col-xs-12">
                    <?php if ($button = $block['announcement']['button']) : ?>
                        <div class="button-container announcement-btn">
                            <a class="btn btn-white-line" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
