<?php
use Cn\Theme;
?>

<div class="background" style="background-image:url(<?= $block['background_image']['url']; ?>">
    <div class="container">
        <h1 class="title"><?= $block['title']; ?></h1>
        <ul class="products">
            <?php foreach ($block['products'] as $product) : ?>
                <li class="product">
                    <div class="flex">
                        <img src="<?= $product['image']['url']; ?>" >
                        <div class="text-container">
                            <h2 class="product-title text-richsred"><?= $product['title']; ?></h2>
                            <div class="text"><?= $product['text']; ?></div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>

            <?php if ($block['use_redirection_button'] === true) : $globalPage = $block['global_page']; $fallback = $block['fallback_page']; ?>
                <li class="cta">
                    <a class="btn btn-red" href="javascript:void(0);" data-regional-link="true" data-global-id="<?= $globalPage->ID; ?>" data-fallback="<?= get_permalink($fallback->ID); ?>"><?= $block['button_text']; ?></a>
                </li>
            <?php elseif ($button = $block['button']) : ?>
                <li class="cta">
                    <a class="btn btn-red" href="<?= $button['url'] ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</div>