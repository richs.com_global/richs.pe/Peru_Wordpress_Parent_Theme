<?php

use Cn\Theme;
    // Get all of the region specific pages for this page
    $regionalPages = get_field('redirect_pages', $block['global_page']->ID);
    
    // Get all of the possible regions
    $regions = Theme::get_all_regions(true);
    
    // For each region, initalize an empty array of pages
    $groupedRegionPages = [];
    foreach($regions as $region){
        $group = get_field('group_label', $region);
        $groupedRegionPages[$group] = [];
    }

    // For reach of the region specific versions of this page
    foreach($regionalPages as $page) {
        // Get the group label for this region version of the page
        $groupLabel = get_field('group_label', $page['region']->ID);
        // Assign this page to that group region
        $groupedRegionPages[$groupLabel][] = $page;
    }

    // The current region name
    if ($block['current_region'] != null) {
        $currentRegionText = $block['current_region']->post_title;
    }
    if ($currentRegionText == null || $currentRegionText == "Global") $currentRegionText = "The Global Rich's Experience";
?>
<div class="container">
    <div class="flex">
        <h2 class="block-title"><?= $block['title']; ?></h2>
        <region-page-dropdown current-region-text="<?= $currentRegionText; ?>" :regional-pages="<?= htmlentities(json_encode($groupedRegionPages), ENT_QUOTES); ?>" global-href="<?= get_permalink($block['global_page']->ID); ?>?location=global">
        
        </region-page-dropdown>

        <?php if($block['secondary'] != "") : ?>
                <h5 class="block-title secondary"><?= $block['secondary']; ?></h5>
        <?php endif; ?>
    </div>
</div>