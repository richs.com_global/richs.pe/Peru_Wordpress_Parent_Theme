<?php
    $region = Cn\Theme::get_current_region();
?>
<div class="container">
    <h2 class="block-title"><?= $block['title']; ?></h2>
    <div class="search-container">
        <form role="search" method="get" class="search-form" action="<?= get_home_url(); ?>">
            <input placeholder="Search for anything" type="text" name="s" value="" class="mb-0" />
            <button type="submit" class="btn">Search <i class="fas fa-chevron-right"></i></button>
        </form>
        <span class="block mt-5 product-site" style="display: none;"> Looking for a specific product? Visit the 
                <a class="hover:text-black text-richsred" data-regional-marketing-link="true"></a> 
            product site</span>
    </div>
</div>