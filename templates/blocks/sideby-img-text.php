<div class="container">
        <div class="content-container row">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <div class="text contact-tb"><?= $block['text']; ?></div>
        </div>
    <div class="flex">
        <?php foreach ($block['items'] as $item) : $header = $item['header']; ?>
            <div class="item">
                <div class="header type-<?= $header['type']; ?>">
                    <?php if ($header['type'] == "text") : ?>
                        <div class="value"><?= $header['text']; ?></div>
                    <?php elseif ($header['type'] == "image") : ?>
                        <img src="<?= $header['image']['url']; ?>" alt="<?= $header['image']['alt']; ?>" />
                    <?php endif; ?>
                </div>
                <div class="content">
                    <?php if ($label = $item['label']) : ?>
                        <div class="label"><?= $label; ?></div>
                    <?php endif; ?>
                    <?php if ($text = $item['text']) : ?>
                        <div class="text"><?= $text; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>