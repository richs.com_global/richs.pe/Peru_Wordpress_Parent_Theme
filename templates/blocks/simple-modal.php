<div id="<?= $block['modal_id'];?>" class="modal-overlay">
    <a href="#" class="cancel"></a>

    <div class="modal">
        <div class="content wysiwyg">
            <?= $block['content']; ?>

            <a href="#" class="close">&times;</a>
        </div>
    </div>
</div>