<div class="container">
    <div class="flex">
        <?php foreach ($block['items'] as $item) : ?>
            <div class="item">
                <div class="header">
                    <div class="value"><?= $item['title']; ?></div>
                </div>
                <div class="content">
                    <?php if ($label = $item['label']) : ?>
                        <div class="label"><?= $label; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>