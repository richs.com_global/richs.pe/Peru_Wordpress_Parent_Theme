<?php if($block):
    $brands = $block['brands'];

    $options = [
        'slidesToShow' => 2,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => true,
        'touchThreshold' => 100,
        'infinite' => false,
        'variableWidth' => true,
        'responsive' => [
            [
                'breakpoint' => 1024,
                'settings' => [
                    'slidesToShow' => 4,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 768,
                'settings' => [
                    'slidesToShow' => 2,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 567,
                'settings' => [
                    'slidesToShow' => 2,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                    'adaptiveHeight' => true
                ]
            ]
        ]
    ];
?>

        <?php if ($background_image = $block['background_image']) : ?>
            <div class="background-wrapper testimonial-background-wrapper" style="background-image:url(<?= $block['background_image']['url']; ?>); min-height:<?= $block['background_image']['height'];?>px;">
        <?php endif; ?>


    <div class="brand-carousel testimonial-carousel">
        <div class="container">
            <div class="col-xs-12 col-md-4">
                <?php if ($title = $block['title']) : ?>
                    <h2 class="block-title"><?= $title; ?></h2>
                <?php endif; ?>

                <?php if ($suporting_text = $block['suporting_text']) : ?>
                    <p class="block-suporting-text"><?= $suporting_text; ?></p>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-2"></div>
            <div class="col-xs-12 col-md-6">
                <div class="carousel-container">
                <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel">

                    <?php foreach ($block['brands'] as $brand) : $hoverImage = $brand['hover_image']; ?>
                        <div class="brand">
                            <div class="flex">
                                <div class="testimonial-content">
                                    <div class="copy h-full flex-grow">
                                        <?= $brand['copy']; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="image-container">
                                            <img class="headshot" src="<?= $brand['photo']['url']; ?>" >
                                        </div>
                                    </div>

                                    <div class="col-xs-8">
                                        <div class="name">
                                            <?php if ($name = $brand['name']) : ?>
                                                <p class="block-name"><?= $name; ?></p>
                                            <?php endif; ?>
                                        </div>
                                        <div class="date">
                                            <?php if ($date = $brand['date']) : ?>
                                                <p class="date-name"><?= $date; ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </slick-carousel>
            </div>
            </div>

            

            
            
        </div>
    </div>
<?php endif; ?>