<?php 
use Cn\Theme;
if($block): 

$regions = get_field('regions', get_the_ID());
$countryCodes = "";
$countryCodesArr = array();
if($regions != null){
    foreach($regions as $region){
        if($region->post_name == "global"){
            $posts = Theme::get_all_regions();
            $countryCodesArr = array();
            foreach($posts as $post){
                array_push($countryCodesArr, get_field('country_codes', $post));
            }
            break;
        } else {
            array_push($countryCodesArr, get_field('country_codes', $region->ID));
        }
    }
    $countryCodes = implode(",", $countryCodesArr);
}
?>
<div class="container">
    <div class="flex flex-row content-wrapper image-position-<?= $block['map_position']; ?>">
        <div class="text-container">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <div class="text"><?= $block['text']; ?></div>
            <?php if ($button = $block['button']) : ?>
            <a class="btn btn-red icon-btn" href="<?= $button['url']; ?>"
                target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-chevron-right"></i></a>
            <?php endif; ?>
        </div>
        <div class="image-container">
            <span class="note">Global Availability</span>
            <text-with-map countrycodes="<?= $countryCodes ?>">
                <?php echo file_get_contents(THEME_URL . "/templates/svg/world-map.svg"); ?>
            </text-with-map>
        </div>
    </div>
</div>
<?php endif; ?>