<?php if ($block) : 
    wp_enqueue_style("timeline-css", "//cdn.knightlab.com/libs/timeline3/latest/css/timeline.css", ['main']);
?>
<div class="">
    <div class="<?= $block['style']; ?> container"> 
        <timeline url="<?= $block['google-sheets-url']; ?>"></timeline>
    </div>
</div>
<?php endif; 