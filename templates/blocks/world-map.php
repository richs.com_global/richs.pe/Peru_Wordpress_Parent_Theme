<div class="bg-cover bg-center" style="background-image:url('/wp-content/uploads/2020/02/map_animated_turkey.svg');">
    <div class="<?= $block['style']; ?> container">
        <?php if ($button = $block['button']) : ?>
            <div class="button-container line-red-btn">
                <p class="txt-center"><a class="btn btn-white-line" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a></p>
            </div>
        <?php endif; ?>
    </div>
    </div>