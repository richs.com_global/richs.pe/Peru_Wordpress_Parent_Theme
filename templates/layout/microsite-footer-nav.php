<?php if($links = get_field('microsite_footer_nav')) : ?>
    <ul id="menu-footer-column-1" class="styled-menu menu-1 microsite">
    <?php
        foreach($links as $link) { ?>
            <li class="menu-item"><a href="<?= $link['link']['url']; ?>" target="<?= $link['link']['target']; ?>" ><?= $link['link']['title'];?></a></li>
    <?php } ?>
    </ul>

<?php endif; // end microsite_footer_nav ?>

<?php if(get_field('add_microsite_social')) : ?>
<div class="social-media microsite">
        <ul>
            <?php if ($facebook = get_field('microsite_social')['facebook']) : ?>
                <li><a href="<?= $facebook; ?>" target="_blank"><img alt="Facebook" src="<?= THEME_URL . '/assets/dist/images/logos/facebook-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($instagram = get_field('microsite_social')['instagram']) : ?>
                <li><a  href="<?= $instagram; ?>" target="_blank"><img alt="Instagram" src="<?= THEME_URL . '/assets/dist/images/logos/instagram-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($linkedin = get_field('microsite_social')['linkedin']) : ?>
                <li><a href="<?= $linkedin; ?>" target="_blank"><img alt="LinkedIn" src="<?= THEME_URL . '/assets/dist/images/logos/linkedin-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($twitter = get_field('microsite_social')['twitter']) : ?>
                <li><a href="<?= $twitter; ?>" target="_blank"><img alt="Twitter" src="<?= THEME_URL . '/assets/dist/images/logos/twitter-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($youtube = get_field('microsite_social')['youtube']) : ?>
                <li><a href="<?= $youtube; ?>" target="_blank"><img alt="YouTube" src="<?= THEME_URL . '/assets/dist/images/logos/youtube-logo.png'; ?>" /></a></li>
            <?php endif; ?>
        </ul>
    </div>
<?php endif; //end microsite_footer_social