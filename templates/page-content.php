<?php the_content(); ?>

<?php
    /**
     * If the employee cta is shown on this page, enqueue the script to generate it
     */
    if (get_field('show_employee_cta')) {
        wp_enqueue_script('employee-cta');
    }
?>