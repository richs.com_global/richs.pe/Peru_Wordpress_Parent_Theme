<?php use Cn\Blocks\Video; ?>

<section class="block hero">
    <div class="background" style="background-image:url(<?= THEME_URL . '/assets/dist/images/richs-gradient-bg.jpg'; ?>); max-height: 407px;">
        <div class="container">
            <div class="flex">
                <div class="content">
                    <h2 class="top-title back-to-leadership"><a href="/leaders"><i class="fas fa-chevron-left"></i> Back to leadership</a></h2>
                    <h1 class="title"><?= the_field('first_name'); ?> <?= the_field('last_name'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block bio-section section-1">
    <div class="container">
        <div class="flex">
            <div class="left-container">
                <h2 class="block-title"><?= the_field('job_title'); ?></h2>
                <div class="text">
                    <?= the_field('bio'); ?>
                </div>
            </div>
            <div class="right-container">
                <?php $headshot = get_field('headshot'); ?>
                <img src="<?= $headshot['url']; ?>" alt="<?= $headshot['alt']; ?>" />
                <div class="list">
                    <?php if(!empty(get_field('favorite_meal'))): ?>
                    <div class="item">
                        <h5 class="title">Favorite Meal</h5>
                        <div class="text"><?= the_field('favorite_meal'); ?></div>
                    </div>
                    <?php endif; ?>
                    <?php if(!empty(get_field('favorite_activity'))): ?>
                    <div class="item">
                        <h5 class="title">Favorite Activity</h5>
                        <div class="text"><?= the_field('favorite_activity'); ?></div>
                    </div>
                    <?php endif; ?>
                    <?php if(!empty(get_field('best_part_job'))): ?>
                    <div class="item">
                        <h5 class="title">Best Thing Abour Your Job</h5>
                        <div class="text"><?= the_field('best_part_job'); ?></div>
                    </div>
                    <?php endif; ?>
                    <?php if(!empty(get_field('worst_part_job'))): ?>
                    <div class="item">
                        <h5 class="title">Worst Thing Abour Your Job</h5>
                        <div class="text"><?= the_field('worst_part_job'); ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (get_field('display_featured_media') === true) : ?>
    <?php $type = get_field('media_type'); ?>
            <?php if ($type == 'image') : ?>
                <section class="block featured-media">
                    <div class="container">
                        <img src="<?= get_field('featured_media_image')['url']; ?>" />
                    </div>
                </section>
            <?php elseif($type == 'video') :
                $videoParams = get_field('featured_media_video');
                $data = [
                    'name' => 'video',
                    'youtube_id' => $videoParams['youtube_video_id'],
                    'placeholder_image' => $videoParams['placeholder_image']
                ];
                $video = new Video();
                $video->render_from_code($data);
    ?><?php endif; ?>
<?php endif ?>
<section class="block bio-section section-2">
    <div class="container">
        <div class="flex">
            <div class="left-container md:flex-grow">
                <?php if(!empty(get_field('career_highlights'))): ?>
                    <h2 class="block-title">Career Highlights</h2>
                    <div class="text">
                        <?= the_field('career_highlights'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="right-container">
                <?php $items = get_field('awards_and_honors'); ?>
                <?php if(!empty($items)): ?>
                    <?php if ($items) : ?>
                        <h5 class="title text-richsred">Awards & Honors</h5>
                        <ul class="awards-and-honors">
                            <?php foreach ($items as $item) : ?>
                                <li><?= $item['text']; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="divider">
    <div class="container">
        <hr class="bg-richsred h-1">
    </div>
</section>

<section class="block bio-section section-3">
    <div class="container">
        <div class="flex">
            <div class="left-container md:flex-grow">
                <?php if(!empty(get_field('boards_and_leadership'))): ?>
                    <h2 class="block-title">Boards & Leadership</h2>
                    <div class="text">
                        <?= the_field('boards_and_leadership'); ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="right-container">
                <?php $items = get_field('books_published'); ?>
                <?php if ($items) : ?>
                    <h5 class="title text-richsred">Books Published</h5>
                    <ul class="awards-and-honors">
                        <?php foreach ($items as $item) : ?>
                            <li><?= $item['text']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <div class="button-container">
                    <a href="#" class="btn btn-red btn-print">Print Bio <i class="fas fa-print ml-1"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>